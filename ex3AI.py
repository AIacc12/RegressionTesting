import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import pandas as pd
data = pd.read_csv('ex1data1.txt', sep=',',header=None, names=["population","profit"])
X = data.iloc[:, :-1].values
y = data.iloc[:, 1].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=0)
regresr = LinearRegression()
regresr.fit(X_train, y_train)
print("Linear Intercept",regresr.intercept_)
print("Linear Coeff",regresr.coef_)
plt.subplot(1,2,1)
plt.ylabel('profit')
plt.xlabel('population')
plt.title('Linear Regression for Profit curve')
plt.scatter(X,y)
plt.scatter(X_test,regresr.predict(X_test))
plyfit=PolynomialFeatures(degree=4)
x_poly_train=plyfit.fit_transform(X_train)
x_poly_test=plyfit.fit_transform(X_test)
poly_reg=LinearRegression()
y_poly_test=poly_reg.fit(x_poly_train,y_train)
print("Polynomial intercept:",poly_reg.intercept_)
print("Polynomial Coeff",poly_reg.coef_)
plt.subplot(1,2,2)
plt.ylabel('profit')
plt.xlabel('population')
plt.title('polynomial Regression for Profit curve')
plt.scatter(X,y)
plt.scatter(X_test,poly_reg.predict(x_poly_test))
plt.show()