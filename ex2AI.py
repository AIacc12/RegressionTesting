from sklearn.model_selection import cross_val_predict
from sklearn import linear_model
import matplotlib.pyplot as plt;
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import pandas as pd
data = pd.read_csv('ex1data1.txt', sep=',',header=None, names=["population","profit"])
X = data.iloc[:, :-1].values
y = data.iloc[:, 1].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=0)
regresr = LinearRegression()
regresr.fit(X_train, y_train)
print("Intercept",regresr.intercept_)
print("Coeff",regresr.coef_)
plt.ylabel('profit')
plt.xlabel('population')
plt.title('Linear Regression for Profit curve')
plt.scatter(X,y)
plt.scatter(X_test,regresr.predict(X_test))
plt.show()