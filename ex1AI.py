import matplotlib.pyplot as plt;
import pandas as pd
data = pd.read_csv('ex1data1.txt', sep=',',header=None, names=["population","profit"])
x = data.iloc[:, :-1].values
y = data.iloc[:, 1].values
plt.scatter(x, y, label='profit curve')
plt.ylabel('profit')
plt.xlabel('population')
plt.title('population vs profit')
plt.legend()
plt.show()